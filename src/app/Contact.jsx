import React, {Component} from "react";
import Helmet from "react-helmet";
import styles from 'StylesRoot/styles.pcss';

export default class Contact extends Component {
    render() {
        return (
            <div className={styles.contact}>
                <Helmet title="Contact us"/>
                <h1>Contact</h1>
            </div>
        );
    }
}
