import React, {Component, createContext} from 'react';
import Helmet from "react-helmet";
import {Switch, Route, withRouter} from 'react-router-dom';
import Homepage from './Homepage';
import Contact from './Contact';
import About from './About';
import Menu from './Menu';
import {connect} from "react-redux";
import {Provider} from 'ContextRoot';
import {Decrement, Increment, GetData} from 'ReduxRoot/actions';

class App extends Component {

    increment = () => {
        this.props.increment();
    };

    decrement = () => {
        this.props.decrement();
    };

    getData = () => {
        this.props.getData();
    };

    render() {
        return (
            <Provider value={this.props.counter}>
                <div>
                    <Helmet
                        htmlAttributes={{lang: "en", amp: undefined}} // amp takes no value
                        titleTemplate="%s | React App"
                        titleAttributes={{itemprop: "name", lang: "en"}}
                        meta={[
                            {name: "description", content: "Server side rendering example"},
                            {name: "viewport", content: "width=device-width, initial-scale=1"},
                        ]}
                        link={[{rel: "stylesheet", href: "/dist/styles.css"}]}
                    />
                    <Menu/>
                    <Switch>
                        <Route exact path='/' component={Homepage}/>
                        <Route path='/about' component={About}/>
                        <Route path='/contact' component={Contact}/>
                    </Switch>
                    <div>
                        <button onClick={this.decrement}>-</button>
                        <span>{this.props.counter.count}</span>
                        <button onClick={this.increment}>+</button>
                    </div>
                    <div>
                        <button onClick={this.getData}>GET DATA</button>
                    </div>
                </div>
            </Provider>
        );
    }
}

const mapStateToProps = ({counter}) => {
    return {
        counter,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        increment: () => dispatch(Increment(3)),
        decrement: () => dispatch(Decrement(2)),
        getData: () => dispatch(GetData()),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
