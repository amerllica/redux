import React, {Component} from "react";
import Helmet from "react-helmet";
import styles from 'StylesRoot/styles.pcss';


export default class About extends Component {

    *got(i) {
        yield i + 65465879687634;
    };

    render() {

        const gen = this.got(2);

        return (
            <div className={styles.about}>
                <Helmet title="About us"/>
                <h1>About</h1>
                <div>{
                    gen.next().value
                }</div>
            </div>
        );
    }
}
