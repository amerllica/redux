import React, {Component} from "react";
import Helmet from "react-helmet";
import styles from 'StylesRoot/styles.pcss';
import {Consumer} from 'ContextRoot';

export default class Homepage extends Component {

    render() {
        return (
            <div className={styles.component}>
                <Helmet title="Welcome to our Homepage"/>
                <h1>
                    <span>Homepage: </span>
                    <span>
                        <Consumer>
                            {context => context.count}
                        </Consumer>
                    </span>
                </h1>
            </div>
        );
    };
}




