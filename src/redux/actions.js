export const Increment = (value) => {
    return {
        type: 'INCREMENT',
        value,
    };
};

export const Decrement = (value) => {
    return {
        type: 'DECREMENT',
        value,
    };
};

export const GetData = (data) => {
    return {
        type: 'GET_DATA_SUCCESSFULLY',
        data,
    };
};
