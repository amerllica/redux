import {combineReducers} from 'redux';

const counter = (state = {count: 0}, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return {
                count: state.count + action.value,
            };
        case 'DECREMENT':
            return {
                count: state.count - action.value,
            };
        default:
            return state;
    }
};

const data = (state = {}, action) => {
    switch (action.type) {
        case 'GET_DATA_SUCCESSFULLY':
            return {
                ...state,
                data: action.data
            };
        default:
            return state;
    }
};

export default combineReducers({
    counter,
    data,
});
